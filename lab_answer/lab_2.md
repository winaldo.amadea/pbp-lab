Name : Winaldo Amadea
NPM : 2006520001
Class : KI

### What is the difference between JSON and XML

JSON is a short for JavaScript Object Notation. It is used in a lot of web applications and mobiles to store and send data. It is based on JavaScript programming language and easy to understand and generate.
XML on the other hand is a short for eXtensible Markup Language. It is designed to be self-descriptive so the user can know all the information from the data.
The difference between two of them are:
-JSON:
It supports array
Easy to read
Based on JavaScript
It doesnt support comment
-XML:
It doesn't support array
It is difficult to read and interpret
It supports comment
It has start and end tags

### What is the difference between HTML and XML

HTML is a short for Hypertext Markup Language where it is used to display data and describes the web structure. It is used to create website that can be viewed by anyone with internet access. 
The difference between  HTML and XML are:
-HTML is mainly used for displaying data
-HTML is not case sensitive
-in HTML white spaces are not preserved

-XML is mainly used for transfering data
-XML is case sensitive
-in XML white spaces are preserved
