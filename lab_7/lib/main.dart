import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final appTitle = 'Register Citizen';
    return MaterialApp(
      title: appTitle,
      home: Scaffold(
          appBar: AppBar(
            title: Text(appTitle),
          ),
          body: new Column(children: <Widget>[
            MyCustomForm(),
            MyAlert(),
          ])),
    );
  }
}

class MyCustomForm extends StatefulWidget {
  @override
  MyCustomFormState createState() {
    return MyCustomFormState();
  }
}

class MyCustomFormState extends State<MyCustomForm> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          TextFormField(
              decoration: const InputDecoration(
                icon: const Icon(Icons.phone),
                hintText: 'Enter your Telephone',
                labelText: 'Telephone',
              ),
              validator: (String? value) {
                if (value!.isEmpty) {
                  return "Telehpone can't be empty";
                }

                return null;
              }),
          TextFormField(
              decoration: const InputDecoration(
                icon: const Icon(Icons.password),
                hintText: 'Enter your password',
                labelText: 'Password',
              ),
              validator: (String? value) {
                if (value!.isEmpty) {
                  return "Password can't be empty";
                }

                return null;
              }),
          TextFormField(
              decoration: const InputDecoration(
                icon: const Icon(Icons.person),
                hintText: 'Enter your name',
                labelText: 'Name',
              ),
              validator: (String? value) {
                if (value!.isEmpty) {
                  return "Name can't be empty";
                }

                return null;
              }),
          TextFormField(
              decoration: const InputDecoration(
                icon: const Icon(Icons.phone),
                hintText: 'Enter a NIK',
                labelText: 'NIK',
              ),
              validator: (String? value) {
                if (value!.isEmpty) {
                  return "NIK can't be empty";
                }

                return null;
              }),
          TextFormField(
              decoration: const InputDecoration(
                icon: const Icon(Icons.phone),
                hintText: 'Enter your SKTM LINK',
                labelText: 'SKTM',
              ),
              validator: (String? value) {
                if (value!.isEmpty) {
                  return "SKTM LINK can't be empty";
                }

                return null;
              }),
          TextFormField(
              decoration: const InputDecoration(
                icon: const Icon(Icons.phone),
                hintText: 'Enter your buku tabungan',
                labelText: 'Buku Tabungan',
              ),
              validator: (String? value) {
                if (value!.isEmpty) {
                  return "Buku tabungan can't be empty";
                }

                return null;
              }),
          TextFormField(
              decoration: const InputDecoration(
                icon: const Icon(Icons.phone),
                hintText: 'Enter your area name',
                labelText: 'Area',
              ),
              validator: (String? value) {
                if (value!.isEmpty) {
                  return "Area name can't be empty";
                }
                return null;
              }),
        ],
      ),
    );
  }
}

class MyAlert extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: RaisedButton(
        child: Text('Submit'),
        onPressed: () {
          showAlertDialog(context);
        },
      ),
    );
  }
}

showAlertDialog(BuildContext context) {
  // Create button
  Widget okButton = FlatButton(
    child: Text("OK"),
    onPressed: () {
      Navigator.of(context).pop();
    },
  );

  // Create AlertDialog
  AlertDialog alert = AlertDialog(
    title: Text("Notification"),
    content: Text("Input has been submitted"),
    actions: [
      okButton,
    ],
  );

  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}
