from django.db import models

# Create your models here.

class Note(models.Model):
    to = models.CharField(max_length=1000)
    from_1 = models.CharField(max_length=1000)
    titles = models.CharField(max_length=1000)
    message = models.CharField(max_length=1000)
    # TODO Implement missing attributes in Friend model