# Generated by Django 3.2.7 on 2021-09-23 17:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lab_2', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='note',
            name='message',
            field=models.CharField(max_length=1000),
        ),
        migrations.AlterField(
            model_name='note',
            name='titles',
            field=models.CharField(max_length=1000),
        ),
    ]
